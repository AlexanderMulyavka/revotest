package com.revolut.currency.di

import com.revolut.currency.CurrencyApplication
import com.revolut.currency.presentation.fragment.CurrencyFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [AppModule::class])
interface AppComponent {
    fun inject(currencyApplication: CurrencyApplication)
    fun inject(currencyFragment: CurrencyFragment)
}