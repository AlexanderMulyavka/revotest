package com.revolut.currency

import android.app.Application
import com.revolut.currency.di.AppComponent
import com.revolut.currency.di.AppModule
import com.revolut.currency.di.DaggerAppComponent

class CurrencyApplication: Application() {

    val appComponent: AppComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        injectDependencies()
    }

    fun injectDependencies() =
        appComponent.inject(this)
}