package com.revolut.currency.presentation.presenter

import com.revolut.currency.presentation.view.CurrencyView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class CurrencyPresenter: MvpPresenter<CurrencyView>() {

}