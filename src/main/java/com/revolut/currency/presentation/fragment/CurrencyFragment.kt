package com.revolut.currency.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.revolut.currency.CurrencyApplication
import com.revolut.currency.R
import com.revolut.currency.data.network.ExchangeService
import com.revolut.currency.di.AppComponent
import com.revolut.currency.presentation.presenter.CurrencyPresenter
import com.revolut.currency.presentation.view.CurrencyView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import javax.inject.Inject

class CurrencyFragment : MvpAppCompatFragment(), CurrencyView {

    @InjectPresenter
    lateinit var presenter: CurrencyPresenter

    @Inject
    lateinit var service: ExchangeService

    private val appComponent: AppComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as CurrencyApplication).appComponent
    }

    override fun showToast(msg: String,currency:String ) {
        Toast.makeText(context, msg + currency, Toast.LENGTH_LONG).show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return  inflater.inflate(R.layout.fragment_currency, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appComponent.inject(this)
        service.getExchangeRates()
    }

    companion object {
        fun getInstance() = CurrencyFragment()
    }
}