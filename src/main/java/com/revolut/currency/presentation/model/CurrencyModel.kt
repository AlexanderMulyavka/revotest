package com.revolut.currency.presentation.model

data class CurrencyModel(val name: String, val image: String)