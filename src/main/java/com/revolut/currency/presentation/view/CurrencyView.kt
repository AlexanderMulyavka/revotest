package com.revolut.currency.presentation.view

import com.arellomobile.mvp.MvpView

interface CurrencyView: MvpView {

    fun showToast(msg: String, currency:String = "Безвалюты")
}