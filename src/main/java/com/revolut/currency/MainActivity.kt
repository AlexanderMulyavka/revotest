package com.revolut.currency

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.revolut.currency.presentation.fragment.CurrencyFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showCurrencyFragment()
    }

    fun showCurrencyFragment() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, CurrencyFragment.getInstance())
                .commit()
    }

}
