package com.revolut.currency.data.network.model

import org.simpleframework.xml.*

@Root(name = "gesmes:Envelope", strict = false)
data class ExchangeEntity(
        @field:ElementList(name = "Cube", required = false)
        @param:ElementList(name = "Cube", required = false)
        var Cube: List<Time>
)

data class Time(
        @field:Attribute(name = "time")
        @param:Attribute(name = "time")
        var time: String,

        @field:ElementList(entry = "Cube", inline = true, required = false)
        @param:ElementList(entry = "Cube", inline = true, required = false)
        var currencyList: List<Currency>
)

data class Currency(
        @field:Attribute(name = "rate")
        @param:Attribute(name = "rate")
        var rate: String
)



