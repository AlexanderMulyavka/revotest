package com.revolut.currency.data.network

import com.revolut.currency.data.network.model.ExchangeEntity
import io.reactivex.Observable
import retrofit2.http.GET

interface ExchangeApi {

    @GET("eurofxref-daily.xml") fun getExchangeRates(): Observable<ExchangeEntity>
}