package com.revolut.currency.data.network

import com.revolut.currency.data.network.model.ExchangeEntity
import com.revolut.currency.data.prefs.PreferencesHelper
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

class ExchangeService @Inject constructor(val retrofit: Retrofit, val preferencesHelper: PreferencesHelper) : ExchangeApi {
    override fun getExchangeRates(): Observable<ExchangeEntity> {
        var exchange = retrofit.create(ExchangeApi::class.java).getExchangeRates()
        exchange
                .subscribeOn(Schedulers.io())
                .doOnComplete { preferencesHelper.lastCacheTime = System.currentTimeMillis() }
                .subscribe {resp -> resp }

        return exchange
    }

}