package com.revolut.currency.data.prefs

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesHelper @Inject constructor(context: Context) {

    companion object {
        private const val PREF_BUFFER_PACKAGE_NAME = "com.revolut.currency.prefs"
        private const val PREF_KEY_LAST_CACHE = "last_cache"
    }

    private val currencyPref: SharedPreferences

    init {
        currencyPref = context.getSharedPreferences(PREF_BUFFER_PACKAGE_NAME, Context.MODE_PRIVATE)
    }

    var lastCacheTime: Long
        get() = currencyPref.getLong(PREF_KEY_LAST_CACHE, 0L)
        set(lastCache) = currencyPref.edit().putLong(PREF_KEY_LAST_CACHE, lastCache).apply()

}
