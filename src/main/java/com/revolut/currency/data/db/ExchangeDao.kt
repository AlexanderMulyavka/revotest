package com.revolut.currency.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.revolut.currency.data.db.model.ExchangeEntityDB

@Dao
interface ExchangeDao {
    @Query("SELECT * FROM exchangeentitydb")
    fun getCurrencyExchange(): List<ExchangeEntityDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveCurrencyExchange(list: List<ExchangeEntityDB>)
}