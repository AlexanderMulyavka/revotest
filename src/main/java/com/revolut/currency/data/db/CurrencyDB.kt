package com.revolut.currency.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.revolut.currency.data.db.model.ExchangeEntityDB

@Database(entities = arrayOf(ExchangeEntityDB::class), version = 1)
abstract class CurrencyDB : RoomDatabase() {
    abstract fun exchangeDao(): ExchangeDao
}