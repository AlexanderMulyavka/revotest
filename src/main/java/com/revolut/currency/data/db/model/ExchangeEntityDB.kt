package com.revolut.currency.data.db.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity
data class ExchangeEntityDB(
        @PrimaryKey
        @ColumnInfo(name = "id") val id:Int,
        @ColumnInfo(name = "currency") val firstName: String?,
        @ColumnInfo(name = "rate") val lastName: String?
)