package com.revolut.currency.domain

import com.revolut.currency.presentation.model.CurrencyModel

interface CurrencyRepository {
    fun getCurrenciesExchangeRates(): List<CurrencyModel>
    fun getCurrecyExchangeRates(currencyModel: CurrencyModel)
}